# Procedure to migrate Wordpress
## This procedure aims to fully migrate wordpress website (data, theme, extensions, plugins, pages etc ...) from a VPS to another VPS


### Setup the new VPS
First run the ansible playbook of the main repo.


### On the old VPS, executes the following commands :
When it is asked, fill your root mysql password defined on the vars/default.yml file.
>> These command must be executed on the old VPS
```
cd
mkdir -p export_files
sudo chown -R $USER:$USER /var/www/website
cd /var/www/website/wordpress
sudo tar -czvf website_wordpress.tar.gz ./
mv website_wordpress.tar.gz ~/export_files
cd
mysqldump -u root -p wordpress > export_files/db_wordpress.sql
```

### Send website 
>> These command must be executed on the old VPS

Replace values with your new VPS user and your new VPS ip :
```
USER_NEW_VPS=
IP_NEW_VPS=
sudo scp -r export_files/ $USER_NEW_VPS@$IP_NEW_VPS:~/
sudo chown -R www-data:www-data /var/www/website
rm -rf export_files
```
### On the new VPS, executes the following commands :
When it is asked, fill your rcs_user mysql password defined on the vars/default.yml file.
>> These command must be executed on the new VPS
```
cd
mysql -u rcs_user -p wordpress < export_files/db_wordpress.sql
```

```
mkdir export_files/website_wordpress && tar -xvf export_files/website_wordpress.tar.gz -C export_files/website_wordpress
sudo chown -R $USER:$USER /var/www/website
rm -rf /var/www/website/wordpress && mkdir /var/www/website/wordpress
cp -R export_files/website_wordpress/* /var/www/website/wordpress/
mysql -u rcs_user -p
```

Executes the following SQL commands :
>> Replace **IP_NEW_SERVER** by the new VPS ip
```sql
USE wordpress
SELECT * FROM wp_options WHERE option_name IN('siteurl','home');
UPDATE wp_options SET option_value='http://{IP_NEW_SERVER}' WHERE option_name = 'siteurl';
UPDATE wp_options SET option_value='http://{IP_NEW_SERVER}' WHERE option_name = 'home';
SELECT * FROM wp_options WHERE option_name IN('siteurl','home');
QUIT;
```

Executes the following commands :
```
rm -rf ~/export_files
sudo chown -R www-data:www-data /var/www/website
```

Go to your browser and type the IP of your new VPS.

Try to go the wp-admin section and try to fill your wordpress ids from your old VPS