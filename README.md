# Ansible - Wordpress

This project aims to fully setup a Wordpress website via Ansible.

## Prerequisites

To run the projet you have to install Ansible

## Setup playbook

To run the ansible playbook you need to define some variables.

Firstly, create a directory named **vars** at the project root and create a file **default.yml** into this directory.

Then, copy-paste the following variables into the file created previously :
```yaml
#MySQL Settings
mysql_root_password: "your_db_root_password"
mysql_db: "your_wordress_mysql_db"
mysql_user: "your_mysql_user"
mysql_password: "your_mysql_password"

#HTTP Settings
http_host: "your_server_ip_or_domain_name"
http_port: "80"
https_port: "443"
```

## Run the playbook

To run the playbook you need to execute the following command :

```bash
ansible-playbook -u <your_server_username> -i <your_server_ip>, [--private-key <path_of_private_key>] playbook.yml
```

Example : 
```
ansible-playbook -u ubuntu -i 14.154.12.15, --private-key myPrivateKey.pem playbook.yml
```